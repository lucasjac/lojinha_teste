# Lojinha-teste

Este projeto tem como finalidade simular as funcionalidades de um backend de uma loja virtual, que permite ao usuario se logar,criar conta, adicionar produtos ao carrinho e navegar pelos produtos das mais variadas formas com o intuito de fazer uma representaçao simples da realidade, esta API apresentara autenticaçoes com jwt rotas protegidas e senhas criptografadas.

## Tabelas

### usuarios

Tabela de usuarios da API:

| campo | descriçao                         |
| ----- | --------------------------------- |
| id    | chave primaria do usuario         |
| nome  | nome do usuario(requerido)        |
| email | email unico do usuario(requerido) |

### produtos

Tabela de produtos da API:

| campo     | descriçao                               |
| --------- | --------------------------------------- |
| id        | chave primaria do produto               |
| nome      | nome do produto(requerido)              |
| preço     | preço do produto(requerido)             |
| img       | url da imagem do produto(opcional)      |
| categoria | chave estrangeira do produto(requerido) |
| desc      | descrição do produto(opcional)          |

### categorias

Tabela que classifica os produtos

| campo | descriçao                        |
| ----- | -------------------------------- |
| id    | chave primaria da categoria      |
| nome  | nome da categoria(requerido)     |
| desc  | descrição da categoria(opcional) |

### carrinho

Tabela N:N de ligação do usuario com o produto:

| campo      | descriçao                           |
| ---------- | ----------------------------------- |
| id         | chave primaria do carrinho          |
| usuario_id | identificador do usuario(requerido) |
| produto_id | identificador do produto(requerido) |

## Rotas

### [http://lojnha-teste.herokuapp.com/users/](http://lojnha-teste.herokuapp.com/users/)

Rota protegida por Bearer token, que retorna as informações do usuario:

### Metodo [GET]

#### Resposta:

```
{
  "id": 1,
  "nome": "Lucas",
  "email": "lucasjacinto18@gmail.com",
  "carrinho": "http://lojnha-teste.herokuapp.com/users/cart"
}
```

### [http://lojnha-teste.herokuapp.com/users/signup](http://lojnha-teste.herokuapp.com/users/signup)

Rota desprotegida, de criaçao de usuarios

### Metodo [POST]

#### Resposta:

```
{
  "msg":"User created",
  "login":"http://lojnha-teste.herokuapp.com/users/login"
}
```

### [http://lojnha-teste.herokuapp.com/users/login](http://lojnha-teste.herokuapp.com/users/login)

Rota desprotegida de login de usuarios

### Metodo [POST]

#### Resposta:

```
{
 "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJmcmVzaCI6ZmFsc2UsImlhdCI6MTYyNjExMzkwMiwianRpIjoiNDRhMGNjYzItODM5Ni00ZmFmLWE4YWItYjI4YzdiMzc3NjJjIiwidHlwZSI6ImFjY2VzcyIsInN1YiI6Imx1Y2FzamFjaW50bzE4QGdtYWlsLmNvbSIsIm5iZiI6MTYyNjExMzkwMiwiZXhwIjoxNjI2MTE0ODAyfQ.BqZ0oQRzpzLrRFbh59knf59aRgHBDjKeG6HAIS9SJlk"
}
```

### [https://lojnha-teste.herokuapp.com/products/](https://lojnha-teste.herokuapp.com/products/)

Rota desprotegida de obtençao de produtos, esta pode ser paginada colocando-se numeros ao seu final da seguinte forma "1/20", onde o primeiro indica o numero da pagina e o segundo a quantidade de produtos por pagina, ela tambem pode ser filtrada por nome ao se inserir o nome do produto ao final do endpoint

### Metodo [GET]

#### Resposta:

```
{
 "produtos": [
    {
      "id": 9,
      "nome": "Controle Arcade Turbo Zero Delay Usb Pc Ps3",
      "preço": 198,
      "img": "https://http2.mlstatic.com/D_NQ_NP_858809-MLB43471970686_092020-O.webp",
      "desc": "Controle arcade , feito manualmente caseiro ótima jogabilidade manche comando aegir botões aegir. ",
      "categoria": 1
    },
    {
      "id": 10,
      "nome": "Controle Usb Tipo Sega Saturn Branco",
      "preço": 42,
      "img": "https://http2.mlstatic.com/D_NQ_NP_864032-MLB46389472366_062021-O.webp",
      "desc": "Este é um joystick clássico tipo Sega Saturn a interface é USB conector, é um controle especialmente projetado para jogar jogos da sega no computador/mac/raspberry pi, você tem o joystick, você pode jogar todos os seus games favoritos sobre qualquer sistema de PC ou MAC e relembre os bons momentos de sua infância.",
      "categoria": 1
    }
  ]
}
```

### [https://lojnha-teste.herokuapp.com/products/categorys](https://lojnha-teste.herokuapp.com/products/categorys)

Rota de obtenção de categorias da API, esta pode ser filtrada por nome de maneira semelhante a rota de produtos.

### Metodo [GET]

#### Resposta:

```
{
 "categorias": [
    {
      "id": 1,
      "nome": "Tecnologia",
      "desc": "Artigos eletronicos diversos"
    },
    {
      "id": 2,
      "nome": "Moda,beleza e perfumaria",
      "desc": "Artigos de moda, maquiagem e beleza"
    }
  ]
}
```

### [http://lojnha-teste.herokuapp.com/users/cart](http://lojnha-teste.herokuapp.com/users/cart)

Rota de obtenção de Obtenção de produtos do usuario esta rota e protegida por Bearrer token.

### Metodo [GET]

#### Resposta:

```
{
 "carrinho": [
    {
      "id": 9,
      "nome": "Controle Arcade Turbo Zero Delay Usb Pc Ps3",
      "preço": 198,
      "img": "https://http2.mlstatic.com/D_NQ_NP_858809-MLB43471970686_092020-O.webp",
      "desc": "Controle arcade , feito manualmente caseiro ótima jogabilidade manche comando aegir botões aegir. ",
      "categoria": 1
    }
  ]
}
```

### [http://lojnha-teste.herokuapp.com/users/cart](http://lojnha-teste.herokuapp.com/users/cart)

Rota de inserção de produtos do usuario no carrinho esta rota e protegida por Bearrer token e o id do produto deve ser enviado num json com o nome "produtos_id".

### Metodo [POST].

#### Resposta:

```
Product inserted on cart
```

### [http://lojnha-teste.herokuapp.com/users/cart/remove](http://lojnha-teste.herokuapp.com/users/cart/remove)

Rota de inserção de produtos do usuario no carrinho esta rota e protegida por Bearrer token e o id do produto deve ser enviado num json com o nome "produto".

### Metodo [DELETE].

#### Resposta:

Resposta sem corpo
