from app.configs.database import db

class Produtos(db.Model):
    __tablename__ = "produtos"
    id=db.Column(db.Integer,primary_key=True)
    nome=db.Column(db.String(100),nullable=False,unique=True)
    preço=db.Column(db.Integer,nullable=False)
    img=db.Column(db.Text())
    categoria=db.Column(db.Integer,db.ForeignKey("categorias.id"),nullable=False)
    desc=db.Column(db.Text())
    produtos_usuario=db.relationship("Usuarios",secondary="carrinho",backref="produtos_list")