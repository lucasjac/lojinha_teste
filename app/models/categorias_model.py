from app.configs.database import db 

class Categorias(db.Model):
    __tablename__ = "categorias"
    id=db.Column(db.Integer,primary_key=True)
    nome=db.Column(db.String(100),nullable=False,unique=True)
    desc=db.Column(db.Text())
    produtos=db.relationship("Produtos",backref="categorias")