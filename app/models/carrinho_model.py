from app.configs.database import db

class Carrinho(db.Model):
    __tablename__="carrinho"
    id=db.Column(db.Integer,primary_key=True)
    usuario_id=db.Column(db.Integer,db.ForeignKey("usuarios.id"))
    produtos_id=db.Column(db.Integer,db.ForeignKey("produtos.id"))