from flask import Flask,Blueprint,request,jsonify
from app.models.usuarios_model import Usuarios
from app.models.carrinho_model import Carrinho
from app.models.produtos_model import Produtos
from app.configs.database import db
from http import HTTPStatus
from flask_jwt_extended import create_access_token, get_jwt_identity, jwt_required

bp=Blueprint("bp_users",__name__,url_prefix="/users")

@bp.route("/test")
def test():
    return {"message":"aplicaçao rodando"},HTTPStatus.OK

@bp.route("/")
@jwt_required()
def get_user():
    current_user = get_jwt_identity()
    user = Usuarios.query.filter_by(email=current_user).first()

    return {"id":user.id, "nome":user.nome,"email":user.email,"carrinho":"http://lojnha-teste.herokuapp.com/users/cart"},HTTPStatus.OK

@bp.route("/signup",methods=["POST"])
def create_user():
    user_data=request.get_json()
    password_to_hash = user_data.pop("password")
    new_user=Usuarios(**user_data)
    new_user.password = password_to_hash
    db.session.add(new_user)
    db.session.commit()
    return {"msg":"User created","login":"http://lojnha-teste.herokuapp.com/users/login"},HTTPStatus.CREATED

@bp.route("/login",methods=["POST"])
def login_user():
    user_data=request.get_json()
    found_user = Usuarios.query.filter_by(email=user_data["email"]).first()
    if not found_user:
        return {"message": "User not found"}, HTTPStatus.NOT_FOUND
    if found_user.verify_password(user_data["password"]):
       access_token = create_access_token(identity=found_user.email)
       return jsonify(access_token=access_token), HTTPStatus.OK
    else:
        return {"message": "Unauthorized"}, HTTPStatus.UNAUTHORIZED    

@bp.route("/cart")
@jwt_required()
def get_cart():    
   current_user = get_jwt_identity()
   user = Usuarios.query.filter_by(email=current_user).first()
   if user: 
       carrinho=Carrinho.query.filter_by(usuario_id=user.id).all()
       user_products = []
       for prod in carrinho:
           user_prod = Produtos.query.filter_by(id=prod.produtos_id).first()
           user_products.append(user_prod)
       produtos = [{"id":produto.id,"nome":produto.nome,"preço":produto.preço,"img":produto.img,"desc":produto.desc,"categoria":produto.categoria}for produto in user_products]                
       return {"carrinho":produtos},HTTPStatus.OK   
   return "try to login with an valid user.",HTTPStatus.UNAUTHORIZED    

@bp.route("/cart",methods=["POST"])
@jwt_required()
def insert_cart(): 
    data = request.get_json()            
    current_user = get_jwt_identity()
    user = Usuarios.query.filter_by(email=current_user).first()
    if user: 
            data["usuario_id"] = user.id
            new_product = Carrinho(**data)
            db.session.add(new_product)
            db.session.commit()    
            return "Product inserted on cart",HTTPStatus.CREATED
    return "try to login with an valid user.",HTTPStatus.UNAUTHORIZED    

@bp.route("/cart/remove",methods=["DELETE"])
@jwt_required()
def remove_cart():
    data = request.get_json()            
    current_user = get_jwt_identity()
    user = Usuarios.query.filter_by(email=current_user).first()
    if user:                   
            produto = Carrinho.query.filter_by(produtos_id=data["produto"]).first()            
            query = Carrinho.query.get(produto.id)
            db.session.delete(query)
            db.session.commit()    
            return "",HTTPStatus.NO_CONTENT
    return "try to login with an valid user.",HTTPStatus.UNAUTHORIZED 
