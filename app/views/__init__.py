from flask import Flask

def init_app(app):
    from .users_views import bp as bp_users
    from .products_views import bp as bp_products

    app.register_blueprint(bp_users) 
    app.register_blueprint(bp_products)