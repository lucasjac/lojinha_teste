from flask import Flask,Blueprint,request
from app.models.produtos_model import Produtos
from app.models.usuarios_model import Usuarios
from app.models.categorias_model import Categorias
from app.configs.database import db
from http import HTTPStatus
from flask_jwt_extended import get_jwt_identity, jwt_required

bp=Blueprint("bp_products",__name__,url_prefix="/products")

@bp.route("/")
def get_products():
    query = Produtos.query.all()
    produtos = [{"id":produto.id,"nome":produto.nome,"preço":produto.preço,"img":produto.img,"desc":produto.desc,"categoria":produto.categoria}for produto in query]
    return {"produtos":produtos},HTTPStatus.OK

@bp.route("/<int:page_number>/<int:products_page>")
def products_page(page_number,products_page):
    query = Produtos.query.all()
    produtos = [{"id":produto.id,"nome":produto.nome,"preço":produto.preço,"img":produto.img,"desc":produto.desc,"categoria":produto.categoria}for produto in query]
    primeiro=(page_number-1)*products_page
    ultimo =primeiro+products_page
    return {"produtos":produtos[primeiro:ultimo]},HTTPStatus.OK    

@bp.route("/<string:product_name>")
def get_products_by_name(product_name):
    produtos_selecionados = Produtos.query.filter(Produtos.nome.ilike(f'%{product_name}%')).all()
    produtos = [{"id":produto.id,"nome":produto.nome,"preço":produto.preço,"img":produto.img,"desc":produto.desc,"categoria":produto.categoria}for produto in produtos_selecionados]
    return {"produtos":produtos},HTTPStatus.OK 

@bp.route("/create",methods=["POST"])
@jwt_required()
def create_products():
    data = request.get_json()
    current_user = get_jwt_identity()
    if current_user == "lucasjacinto18@gmail.com":        
        found_category = Categorias.query.filter_by(id=data["categoria"]).first()
        if found_category:
            new_product=Produtos(**data)        
            db.session.add(new_product)
            db.session.commit()
            return "Product created",HTTPStatus.CREATED
        return "Select a valid category",HTTPStatus.UNAUTHORIZED 
    return "try to login with an valid user.",HTTPStatus.UNAUTHORIZED     

@bp.route("/delete",methods=["DELETE"])
@jwt_required()
def delete_products():
    data = request.get_json()            
    current_user = get_jwt_identity()
    if current_user == "lucasjacinto18@gmail.com":
        user = Usuarios.query.filter_by(email=current_user).first()
    if user:                   
            produto = Produtos.query.filter_by(id=data["produto"]).first()            
            query = Produtos.query.get(produto.id)
            db.session.delete(query)
            db.session.commit()    
            return "",HTTPStatus.NO_CONTENT
    return "try to login with an valid user.",HTTPStatus.UNAUTHORIZED 

@bp.route("/categorys")
def get_all_categories():
    query = Categorias.query.all()
    categorias = [{"id":categoria.id,"nome":categoria.nome,"desc":categoria.desc}for categoria in query]
    return {"categorias":categorias},HTTPStatus.OK

@bp.route("/categorys/<string:category_name>")
def get_categories(category_name):     
     categoria = Categorias.query.filter(Categorias.nome.ilike(f'%{category_name}%')).first()              
     if categoria:
        produtos_selecionados = Produtos.query.filter_by(categoria=categoria.id).all()
        produtos = [{"id":produto.id,"nome":produto.nome,"preço":produto.preço,"img":produto.img,"desc":produto.desc,"categoria":produto.categoria}for produto in produtos_selecionados]
        return {"produtos":produtos},HTTPStatus.OK
     else:
         return "Category not found",HTTPStatus.NOT_FOUND      

@bp.route("/create/categorys",methods=["POST"])    
@jwt_required()
def create_categories():
    data = request.get_json()
    current_user = get_jwt_identity()
    if current_user == "lucasjacinto18@gmail.com":
        new_category = Categorias(**data)
        db.session.add(new_category)
        db.session.commit()
        return "Category created",HTTPStatus.CREATED
    return "try to login with an valid user.",HTTPStatus.UNAUTHORIZED