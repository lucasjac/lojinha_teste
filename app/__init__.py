from flask import Flask
from environs import Env
from flask_jwt_extended import JWTManager

from app.configs import database,migrations
from app import views

env = Env()
env.read_env()

def create_app():
    app=Flask(__name__)
    app.config["SQLALCHEMY_DATABASE_URI"]=env("SQLALCHEMY_DATABASE_URI")
    app.config["SQLALCHEMY_TRACK_MODIFICATIONS"]=False
    app.config["JSON_SORT_KEYS"]=False
    # Tente colocar uma secret key para poder instanciar o jwt no init app
    app.config['SECRET_KEY'] = env("SECRET_KEY")    
    
    jwt = JWTManager(app)
    database.init_app(app)
    migrations.init_app(app)
    views.init_app(app)

    return app
