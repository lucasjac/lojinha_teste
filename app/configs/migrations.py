from flask import Flask
from flask_migrate import Migrate

mg = Migrate()

def init_app(app):
    mg.init_app(app,app.db)