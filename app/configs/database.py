from flask import Flask
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()

def init_app(app):
    db.init_app(app)
    app.db=db
    
    from app.models.usuarios_model import Usuarios
    from app.models.categorias_model import Categorias
    from app.models.produtos_model import  Produtos
    from app.models.carrinho_model import Carrinho